% A script that tests a hopfield network for digit (0-9)detection & reconstruction, using different iterations and noise levels

% Error and iteration limits
clear all; close all; clc;

single = true;
chars25 = false;
multiple = false;

if chars25    
    noiselevel = 3;
    num_iters = 1:1:10;
    num_chars = 25;
    img_resize = 1:1:4;
    pixelErrors = zeros(length(img_resize), length(num_iters)); % Stores pixel errors    
    
    for j = 1:length(num_iters)
        for i = 1:length(img_resize)
            pixelErrors(i, j) = hopCharsResize(noiselevel,num_iters(j),num_chars,img_resize(i)); % Store error for current setup
        end
    end
    
    figure
    plot(pixelErrors)
    title(sprintf('Pixel Errors vs Image Scale: Hopfield net (%d pixels noise, %d chars)', noiselevel, num_chars))
    legend( 'Iterations: ' + string(num_iters), 'Location', 'northwest')
    xlabel('Image Scale')
    ylabel('Pixel errors')    
    
    figure
    indices = 1:int16(length(img_resize)/4):length(img_resize);
    plot(pixelErrors(indices,:)')
    title(sprintf('Pixel Errors vs Iterations: Hopfield net (%d pixels noise, %d chars)', noiselevel, num_chars))
    legend('Resize: ' + string(img_resize(indices)), 'Location', 'northeast')
    xlabel('Iterations')
    ylabel('Pixel errors')
    
    figure
    [X,Y] = meshgrid(img_resize, num_iters);
    surf(X,Y,pixelErrors')
    title(sprintf('Pixel Errors vs Iterations vs Scale: Hopfield net (%d pixels noise, %d chars)', noiselevel, num_chars))
    xlabel('Image Scale')
    ylabel('Iterations')
    zlabel('Pixel errors')
    colorbar
end

if single
    pixelErrors = hopChars(3,1,5);
end
if multiple
    noiselevel = 3;
    num_iters = 1:1:10;%num_iters(1) = 1;
    num_chars = 1:1:25;
    pixelErrors = zeros(length(num_chars), length(num_iters)); % Stores pixel errors    
    
    for j = 1:length(num_iters)
        for i = 1:length(num_chars)
            pixelErrors(i, j) = hopChars(noiselevel,num_iters(j),num_chars(i)); % Store error for current setup
        end
    end
    
    figure
    plot(pixelErrors)
    title(sprintf('Pixel Errors vs Characters: Hopfield net (%d pixels noise)', noiselevel))
    legend( 'Iterations: ' + string(num_iters), 'Location', 'northwest')
    xlabel('Characters')
    ylabel('Pixel errors')    
    
    figure
    indices = 1:int16(length(num_chars)/4):length(num_chars);
    plot(pixelErrors(indices,:)')
    title(sprintf('Pixel Errors vs Iterations: Hopfield net (%d pixels noise))', noiselevel))
    legend('Characters: ' + string(num_chars(indices)), 'Location', 'northeast')
    xlabel('Iterations')
    ylabel('Pixel errors')
    
    figure
    [X,Y] = meshgrid(num_chars, num_iters);
    surf(X,Y,pixelErrors')
    title(sprintf('Pixel Errors vs Iterations vs Characters: Hopfield net (%d pixels noise)', noiselevel))
    xlabel('Characters')
    ylabel('Iterations')
    zlabel('Pixel errors')
    colorbar
end