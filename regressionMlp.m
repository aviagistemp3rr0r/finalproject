clear all; close all; clc;
load Data_Problem1_regression.mat

% Student Number: r0692313 -- first 5 sorted DSC ->  9     6     3     3     2
Tnew = (9*T1 + 6*T2 + 3*T3 + 3*T4 + 2*T5) / (9 + 6 + 3 + 3 + 2);

rng('default');rng(33); % Set seed for reproducability
trainingSample = randsample(13600, 1000);
validationSample = randsample(13600, 1000);
testSample = randsample(13600, 1000);

% Do draw training sample surface
plot3dSurfaces = true;

% Params
scatteredInterpolantMethod = 'linear';

if plot3dSurfaces
    figure
    F = scatteredInterpolant(X1(trainingSample),X2(trainingSample),Tnew(trainingSample));
    [xq,yq] = meshgrid(0:0.1:1);
    F.Method = scatteredInterpolantMethod;
    vq1 = F(xq,yq);
    plot3(X1(trainingSample),X2(trainingSample),Tnew(trainingSample),'bx')
    hold on
    mesh(xq,yq,vq1)
    legend('Sample Points','Interpolated Surface','Location','NorthWest')
    grid on;
    title(sprintf('Training data - Tnew vs X1 vs X2: MLP (%d samples, surface: linear)', length(Tnew(trainingSample))))
    xlabel('X1')
    ylabel('X2')
    zlabel('Tnew')
    colorbar
end


% FeedForward Nets

% Params
trainingFunction = 'trainbr';
scatteredInterpolantMethod = 'linear';
trainingEpochs = 20; % 200
neurons = 50;

% Train
x = [X1' ; X2'];
t = Tnew';
net = feedforwardnet(neurons, trainingFunction);
net.trainParam.epochs=trainingEpochs;  % set the number of epochs for the training 
net.divideFcn  = 'divideind' % DO set train/validation/test indices manually
net.divideParam.trainInd = trainingSample;
net.divideParam.valInd  = validationSample;
net.divideParam.testInd = testSample;
net = train(net,x,t);
view(net)
y = net(x);
perf = perform(net,y,t)

% Simulate with Test set
p = [X1(testSample)' ; X2(testSample)'];
simRes = sim(net,p); % simulate the networks with the input vector p
simRes = simRes';

% Plot Test set expected & predicted

figure
F = scatteredInterpolant(X1(testSample),X2(testSample),simRes);
[xq,yq] = meshgrid(0:0.1:1);
F.Method = scatteredInterpolantMethod;
vq1 = F(xq,yq);
plot3(X1(testSample),X2(testSample),simRes,'bx')
hold on
mesh(xq,yq,vq1)
legend('Sample Points','Interpolated Surface','Location','NorthWest')
grid on;
title(sprintf('Test data - Simulated vs Expected: MLP (%d samples, %d epochs, %s)', length(simRes), trainingEpochs, trainingFunction))
xlabel('X1')
ylabel('X2')
zlabel('Tnew')
colorbar
% Super-impose validation predicted & expected Tnew's
plot3(X1(testSample),X2(testSample),Tnew(testSample),'ro')
legend('Simulated Points','Simulated Interpolated Surface', 'Expected Points','Location','NorthWest')