x = [X1(trainingSample)' ; X2(trainingSample)'];
t = Tnew(trainingSample)';
net = feedforwardnet(50, 'trainbr');
net.trainParam.epochs=10;  % set the number of epochs for the training 
net = train(net,x,t);
view(net)
y = net(x);
perf = perform(net,y,t)
simRes =sim(net,[X1(testSample)'; X2(testSample)']); % simulate the networks with the input vector p

