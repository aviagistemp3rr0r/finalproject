%A script to test if 2d Hopfield network can recognize letters of alphabet.
%Custom 10x lower case letters konstantinos theodorakos + capitals -> konstaihedABCDE...

function pixelErrors = hopCharsResize(noiselevel,num_iter,num_chars,resize)

verbose = true;close all

[X,~] = prprobTheodorakos; % Custom 10x lower case letters konstantinos theodorakos + capitals -> konstaihedABCDE...
clear size;
X(X==0)=-1; % Values must be +1 or -1
%-------------------------------------------------------------------------

x = 5 * resize; % from 5
y = 7 * resize; % from 7
xy = x * y;

%Attractors of the Hopfield network
index_dig = 1:num_chars;

digit= X(:,1);
digit = reshape(digit,5,7); 
digit = imresize(digit, resize);
digit = reshape(digit,xy,1); 
digit = digit(:,1);
T = digit;
for i = 2:num_chars
    digit= X(:,i);
    digit = reshape(digit,5,7); 
    digit = imresize(digit, resize);
    digit = reshape(digit,xy,1); 
    digit = digit(:,1);
    T = [T;digit];
end

T(T>0) = 1;
T(T<=0)= -1;

T = reshape(T, xy, num_chars);

%Create network
net = newhop(T);

%Check if digits are attractors
[Y,~,~] = sim(net,num_chars,[],T);
Y = Y';

if verbose 
    figure;

    subplot(num_chars,4,1);

    for i = 1:num_chars
    digit = Y(i,:);
    digit = reshape(digit,x,y)'; 
    subplot(num_chars,4,((i-1)*4)+1);
    imshow(digit)
    if i == 1
        title('Attractors')
    end
    hold on
    end
end


%The plots show that they are attractors.

%------------------------------------------------------------------------

% Random noise - 3 (unique) inverted pixels (from total xy) x 5 characters
indicesToInvert = ones(xy,num_chars);
for i=1:num_chars    
    randIndices = randperm(xy, noiselevel); % 3 unique random nums, from 1 - xy
    for j=1:noiselevel
        indicesToInvert(randIndices(j),i) = -1;
    end
end
Xn = T .* indicesToInvert;
Xn = Xn';

%Show noisy digits:

if verbose 
    subplot(num_chars,4,2);
    
    for i = 1:num_chars
    digit = Xn(index_dig(i),:);
    digit = reshape(digit,x,y)';
    subplot(num_chars,4,((i-1)*4)+2);
    imshow(digit)
    if i == 1 
        title(sprintf('Noisy (%d pixels)', noiselevel))
    end
    hold on
    end
end

%------------------------------------------------------------------------

%See if the network can correct the corrupted digits 


num_steps = num_iter;

Xn = Xn';
Tn = {Xn(:,index_dig)};
[Yn,~,~] = sim(net,{num_chars num_steps},{},Tn);
Yn = Yn{1,num_steps};
Yn = Yn';

% Count Pixel errors (Values must be +1 or -1)
Yrounded = Yn;
Yrounded(Yrounded > 0)=1;
Yrounded(Yrounded < 0)=-1;
pixelErrors = sum(sum(Yrounded ~= Y)); % Count how many Y != Yn -> total pixelErrors

if verbose 
    subplot(num_chars,4,3);
    for i = 1:num_chars
    digit = Yn(i,:);
    digit = reshape(digit,x,y)';
    subplot(num_chars,4,((i-1)*4)+3);
    imshow(digit)
    if i == 1
        title(sprintf('Hopfield (%d steps)', num_iter))
    end
    hold on
    end
end

if verbose 
    subplot(num_chars,4,4);
    for i = 1:num_chars
    digit = Yrounded(i,:);
    digit = reshape(digit,x,y)';
    subplot(num_chars,4,((i-1)*4)+4);
    imshow(digit)
    if i == 1
        title(sprintf('Clipped (%d errors)', pixelErrors))
    end
    hold on
    end
end

%-----------------------------------------------------------------------

