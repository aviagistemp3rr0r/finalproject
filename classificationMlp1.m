clear all; close all; clc;
rng('default');rng(33); % Set seed for reproducibility

% Student Number: r0692313 -- last digit: 3 -> C+ = 6, C- = 7+8 White

M = dlmread('winequality_data\winequality-white.csv',';',1,0); % Read full dataset

% Split to predictors and expected columns
expected = M(:,end);
data = M(:,1:end-1);
% Indices with class 6 -> 0s, with classes 7+8 -> 1s
expectedIndicesCPlus = find(expected == 6); 
expectedIndicesCMinus = find(expected == 7 | expected == 8);
expectedCPlus = expected(expectedIndicesCPlus);
expectedCMinus = expected(expectedIndicesCMinus);
dataCPlus = data(expectedIndicesCPlus,:);
dataCMinus = data(expectedIndicesCMinus,:);
expectedCPlus = zeros(length(expectedCPlus),1);
expectedCMinus = ones(length(expectedCMinus),1);

% Row bind the split data, to get the final matrices
allData = [dataCPlus; dataCMinus];
allExpected = [expectedCPlus; expectedCMinus];

% Divide into train 70% / validation 15 % / test 15% indices
[trainingIndices,validationIndices,testIndices] = dividerand(length(allExpected),0.7,0.15,0.15);

% FeedForward Nets

% Params
trainingFunction = 'trainscg';
trainingEpochs = 200; % 200
neurons = 50;

% Train
x = allData';
t = allExpected';

pca = true;

if pca
     % Perform PCA with 10% max loss    
    [pn,ps1] = mapstd(x); 
    [ptrans,ps2] = processpca(pn,'maxfrac',0.08);
    size(ptrans);
    x = ptrans;
end

net = patternnet(neurons, trainingFunction);
net.trainParam.epochs=trainingEpochs;  % set the number of epochs for the training 
net.divideFcn  = 'divideind' % DO set train/validation/test indices manually


% Setup Division of Data for Training, Validation, Testing
% For a list of all data division functions type: help nndivision
% net.divideFcn = 'dividerand';  % Divide data randomly
net.divideMode = 'sample';  % Divide up every sample
% net.divideParam.trainRatio = 70/100;
% net.divideParam.valRatio = 15/100;
% net.divideParam.testRatio = 15/100;
net.performFcn = 'crossentropy';  % Cross-Entropy

net.divideParam.trainInd = trainingIndices;
net.divideParam.valInd  = validationIndices;
net.divideParam.testInd = testIndices;
[net,tr] = train(net,x,t);
% view(net)

% TODO: Validation CCR
p = x(:,validationIndices);
simRes = sim(net,p); % simulate the networks with the input vector p
expectedCCR = t(:,validationIndices);
simRes(simRes > 0.5) = 1;
simRes(simRes <= 0.5) = 0;
CCRvalidation = (sum(simRes == expectedCCR) / length(expectedCCR)) * 100 % CCR (11 vars): 77.66%, PCA CCR(6 vars): 72.95%

% Test the Network
y = net(x);
e = gsubtract(t,y);
performance = perform(net,t,y)
tind = vec2ind(t);
yind = vec2ind(y);
percentErrors = sum(tind ~= yind)/numel(tind);

% Recalculate Training, Validation and Test Performance
trainTargets = t .* tr.trainMask{1};
valTargets = t .* tr.valMask{1};
testTargets = t .* tr.testMask{1};
trainPerformance = perform(net,trainTargets,y)
valPerformance = perform(net,valTargets,y)
testPerformance = perform(net,testTargets,y)

% View the Network
% view(net)

% Plots
% Uncomment these lines to enable various plots.
figure, plotperform(tr)
figure, plottrainstate(tr)
figure, ploterrhist(e)
figure, plotconfusion(t,y)
figure, plotroc(t,y)
