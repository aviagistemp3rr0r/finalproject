clear all; close all; clc;
rng('default');rng(33); % Set seed for reproducibility

% Student Number: r0692313 -- last digit: 3 -> C+ = 6, C- = 7+8 White

M = dlmread('winequality_data\winequality-white.csv',';',1,0); % Read full dataset

% Split to predictors and expected columns
expected = M(:,end);
data = M(:,1:end-1);
% Indices with class 6 -> 0s, with classes 7+8 -> 1s
expectedIndicesCPlus = find(expected == 6); 
expectedIndicesCMinus = find(expected == 7 | expected == 8);
expectedCPlus = expected(expectedIndicesCPlus);
expectedCMinus = expected(expectedIndicesCMinus);
dataCPlus = data(expectedIndicesCPlus,:);
dataCMinus = data(expectedIndicesCMinus,:);
expectedCPlus = zeros(length(expectedCPlus),1);
expectedCMinus = ones(length(expectedCMinus),1);

% Row bind the split data, to get the final matrices
allData = [dataCPlus; dataCMinus];
allExpected = [expectedCPlus; expectedCMinus];

% Divide into train 70% / validation 15 % / test 15% indices
[trainingIndices,validationIndices,testIndices] = dividerand(length(allExpected),0.7,0.15,0.15);

% FeedForward Nets

% Params
trainingFunction = 'trainbr';
trainingEpochs = 2; % 200
neurons = 50;

% Train
x = allData';
t = allExpected';

net = patternnet(neurons, trainingFunction);
net.trainParam.epochs=trainingEpochs;  % set the number of epochs for the training 
net.divideFcn  = 'divideind' % DO set train/validation/test indices manually


% Setup Division of Data for Training, Validation, Testing
% For a list of all data division functions type: help nndivision
% net.divideFcn = 'dividerand';  % Divide data randomly
net.divideMode = 'sample';  % Divide up every sample
% net.divideParam.trainRatio = 70/100;
% net.divideParam.valRatio = 15/100;
% net.divideParam.testRatio = 15/100;
net.performFcn = 'crossentropy';  % Cross-Entropy

net.divideParam.trainInd = trainingIndices;
net.divideParam.valInd  = validationIndices;
net.divideParam.testInd = testIndices;
net = train(net,x,t);
view(net)

% Test the Network
y = net(x);
e = gsubtract(t,y);
performance = perform(net,t,y)
tind = vec2ind(t);
yind = vec2ind(y);
percentErrors = sum(tind ~= yind)/numel(tind);

% Recalculate Training, Validation and Test Performance
trainTargets = t .* tr.trainMask{1};
valTargets = t .* tr.valMask{1};
testTargets = t .* tr.testMask{1};
trainPerformance = perform(net,trainTargets,y)
valPerformance = perform(net,valTargets,y)
testPerformance = perform(net,testTargets,y)

% View the Network
view(net)

% Plots
% Uncomment these lines to enable various plots.
%figure, plotperform(tr)
%figure, plottrainstate(tr)
%figure, ploterrhist(e)
%figure, plotconfusion(t,y)
%figure, plotroc(t,y)

% Simulate with Test set
% p = allData(testIndices);
% simRes = sim(net,p); % simulate the networks with the input vector p
% simRes = simRes';


% TODO: Train with "train", CCR with "Validate" set
% TODO: set train/validation/test ratios: 70/15/15
% TODO: set trainfunction (and transfer function? )
% TODO: Function for calc ccr? (or in patternet/nntool exists already?)
% TODO: Eigenvalue decomp all train/validation/test data -> pcaReduced train/validation/test data
% TODO: Binary confusion matrices