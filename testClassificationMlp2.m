clear all; close all; clc;
rng('default');rng(33); % Set seed for reproducibility

% Student Number: r0692313 -- last digit: 3 -> C+ = 6, C- = 7+8 White

M = dlmread('winequality_data\winequality-white.csv',';',1,0); % Read full dataset

% Split to predictors and expected columns
expected = M(:,end);
data = M(:,1:end-1);
% Indices with class 6 -> 0s, with classes 7+8 -> 1s
expectedIndicesCPlus = find(expected == 6); 
expectedIndicesCMinus = find(expected == 7 | expected == 8);
expectedCPlus = expected(expectedIndicesCPlus);
expectedCMinus = expected(expectedIndicesCMinus);
dataCPlus = data(expectedIndicesCPlus,:);
dataCMinus = data(expectedIndicesCMinus,:);
expectedCPlus = zeros(length(expectedCPlus),1);
expectedCMinus = ones(length(expectedCMinus),1);

% Row bind the split data, to get the final matrices
allData = [dataCPlus; dataCMinus];
allExpected = [expectedCPlus; expectedCMinus];

x = allData';
t = allExpected';

pca = true;

if pca     
    x = allData';
    t = allExpected';
    
    % TODO: Perform PCA with 10% max loss    
    [pn,ps1] = mapstd(x);
    [ptrans,ps2] = processpca(pn,'maxfrac',0.1);    
    x = ptrans;
end

% Choose a Training Function
% For a list of all training functions type: help nntrain
% 'trainlm' is usually fastest.
% 'trainbr' takes longer but may be better for challenging problems.
% 'trainscg' uses less memory. Suitable in low memory situations.
trainFcn = 'trainscg';  % Scaled conjugate gradient backpropagation.

% Create a Pattern Recognition Network
hiddenLayerSize = 50;
net = patternnet(hiddenLayerSize, trainFcn);

% Choose Input and Output Pre/Post-Processing Functions
% For a list of all processing functions type: help nnprocess
net.input.processFcns = {'removeconstantrows','mapminmax'};

% Divide into train 70% / validation 15 % / test 15% indices
net.divideFcn = 'dividerand';  % Divide data randomly
net.divideMode = 'sample';  % Divide up every sample
net.divideParam.trainRatio = 70/100;
net.divideParam.valRatio = 15/100;
net.divideParam.testRatio = 15/100;

net.performFcn = 'crossentropy';  % Performance Function: Cross-Entropy

% Choose Plot Functions
net.plotFcns = {'plotperform','plottrainstate','ploterrhist', ...
    'plotconfusion', 'plotroc'};

% Train the Network
[net,tr] = train(net,x,t);

% Test the Network
y = net(x);
e = gsubtract(t,y);
performance = perform(net,t,y)
tind = vec2ind(t);
yind = vec2ind(y);
percentErrors = sum(tind ~= yind)/numel(tind);

% Recalculate Training, Validation and Test Performance
trainTargets = t .* tr.trainMask{1};
valTargets = t .* tr.valMask{1};
testTargets = t .* tr.testMask{1};
trainPerformance = perform(net,trainTargets,y)
valPerformance = perform(net,valTargets,y)
testPerformance = perform(net,testTargets,y)

% View the Network
view(net)

% Plots
% Uncomment these lines to enable various plots.
figure, plotperform(tr)
figure, plottrainstate(tr)
figure, ploterrhist(e)
figure, plotconfusion(t,y)
figure, plotroc(t,y)

